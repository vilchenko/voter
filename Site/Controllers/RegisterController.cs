﻿using System.Linq;
using System.Web.Mvc;
using Site.Models;

namespace Site.Controllers
{
    public class RegisterController : Controller
    {
        [HttpGet]
        public ActionResult Register()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Register(RegisterUserModel user)
        {
            var errors = ModelState
                        .Where(x => x.Value.Errors.Count > 0)
                        .Select(x => new { x.Key, x.Value.Errors })
                        .ToArray();
            if (ModelState.IsValid)
            {
                if (user.Password != user.ConfirmPassword)
                {
                    ModelState.AddModelError("", "Please enter your password once again");
                }
                else
                {
                    using (var dc = new DatabaseVotesEntities())
                    {
                        var v = dc.Users.FirstOrDefault(a => a.Login.Equals(user.Login));
                        if (v != null)
                        {
                            ModelState.AddModelError("", "User with such Login is already exist. Please enter another Login.");
                        }
                        else
                        {
                            var newUser = new User
                                          {
                                              Login     = user.Login,
                                              Password  = user.Password,
                                              FirstName = user.FirstName,
                                              LastName  = user.LastName
                                          };
                            dc.Users.Add(newUser);
                            dc.SaveChanges();

                            return RedirectToAction("Login", "User");
                        }
                    }
                }
            }

            return View(user);
        }
    }
}