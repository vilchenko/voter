﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Site.Models;

namespace Site.Controllers
{
    public class HomeController : Controller
    {
        //
        // GET: /Home/

        public ActionResult Index()
        {
            if (!string.IsNullOrEmpty((string)Session[SessionConst.UserLogin]))
                return View();

            return RedirectToAction("Login", "User");
        }

    }
}
