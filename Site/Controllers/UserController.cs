﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using Site.Models;

namespace Site.Controllers
{
    public class UserController : Controller
    {
        //
        // GET: /User/
        public ActionResult Index()
        {
            return View();
        }

        [HttpGet]
        public ActionResult Login()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Login(User user)
        {
            var errors = ModelState
                        .Where(x => x.Value.Errors.Count > 0)
                        .Select(x => new { x.Key, x.Value.Errors })
                        .ToArray();
            if (ModelState.IsValid)
            {
                using (var dc = new DatabaseVotesEntities())
                {
                    var v = dc.Users.FirstOrDefault(a => a.Login.Equals(user.Login) && a.Password.Equals(user.Password));
                    if (v != null)
                    {
                        Session[SessionConst.UserLogin] = v.Login;
                        Session[SessionConst.UserFullName] = string.Format("{0} {1}", v.FirstName.Trim(), v.LastName.Trim());
                        Session[SessionConst.IsAdmin] = v.IsAdmin;
                        return RedirectToAction("Index", "Home");
                    }
                    else
                    {
                        ModelState.AddModelError("", "Login data is incorrect!");
                    }
                }
            }
            return View(user);
        }

        public ActionResult Logout()
        {
            Session[SessionConst.UserLogin] = string.Empty;
            Session[SessionConst.UserFullName] = string.Empty;
            Session[SessionConst.IsAdmin] = false;

            FormsAuthentication.SignOut();
            return RedirectToAction("Login", "User");
        }

        public ActionResult Register()
        {
            return RedirectToAction("Register", "Register");
        }
    }
}
