﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Site.Models
{
    public class SessionConst
    {
        public const string UserLogin = "UserLogin";

        public const string UserFullName = "UserFullName";

        public const string IsAdmin = "IsAdmin";
    }
}