﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Site.Models
{
    public class RegisterUserModel
    {
        [Required(ErrorMessage = "Please Enter Your First name")]
        public string FirstName { get; set; }

        [Required(ErrorMessage = "Please Enter Your Last name")]
        public string LastName { get; set; }

        [Required(ErrorMessage = "Please Enter Your Login")]
        public string Login { get; set; }

        [Required(ErrorMessage = "Please Enter Your Password")]
        public string Password { get; set; }

        [Required(ErrorMessage = "Please Confirm Your Password")]
        public string ConfirmPassword { get; set; }
    }
}